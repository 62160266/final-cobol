       IDENTIFICATION DIVISION. 
       PROGRAM-ID. FINAL-COBOL.
       AUTHOR. THUN.
       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT TRADE-FILE ASSIGN TO "trader6.dat"
            ORGANIZATION IS LINE SEQUeNTIAL.

       DATA DIVISION.
       FILE SECTION. 
       FD  TRADE-FILE.
       01  TRADE-DETAIL.
           88 END-OF-TRADE-FILE VALUE HIGH-VALUE .
           05 PROVINCE          PIC 99.
           05 TRADER-ID         PIC 9(4).
           05 INCOME            PIC 9(6).

       WORKING-STORAGE SECTION. 
       01  HEAD-LINE            PIC X(50) VALUE 
      -    "PROVINCE    P INCOME    MEMBER  MEMBER INCOME".
       01  ALL-INCOME           PIC 9(11).
       01  PROVINCE-ID          PIC 9(2)  VALUE 01.
       01  MAX-PROVINCE         PIC 9(2)  VALUE ZERO .
       01  MAX-INCOME           PIC 9(3),9(3),9(3)  VALUE ZERO .
       01  MAX-MEMBER           PIC 9(4) VALUE ZERO .
       01  MAX-MEMBER-INCOME    PIC 9(3),9(3) VALUE ZERO .
           

       PROCEDURE DIVISION .
       BEGIN.
           OPEN INPUT TRADE-FILE
           DISPLAY HEAD-LINE
           PERFORM READ-LINE 
           PERFORM PROCESS-LINE UNTIL END-OF-TRADE-FILE
           PERFORM DISPLAY-PROVINCE
           DISPLAY "MAX-PROVINCE: " MAX-PROVINCE " SUM INCOME: $" 
      -     MAX-INCOME  
           CLOSE TRADE-FILE 
           GOBACK 
       .

       READ-LINE.
           READ TRADE-FILE
           AT END SET END-OF-TRADE-FILE TO TRUE
           END-READ
           .

       PROCESS-LINE.
           IF PROVINCE-ID = PROVINCE THEN
              ADD INCOME TO ALL-INCOME
              IF MAX-MEMBER-INCOME < INCOME
                 COMPUTE MAX-MEMBER-INCOME = INCOME 
                 COMPUTE MAX-MEMBER = TRADER-ID  
              END-IF 
           ELSE
              PERFORM DISPLAY-PROVINCE
              IF ALL-INCOME>MAX-INCOME  
                 COMPUTE MAX-INCOME = ALL-INCOME 
                 COMPUTE MAX-PROVINCE = PROVINCE 
              END-IF
              COMPUTE PROVINCE-ID = PROVINCE-ID + 1
              COMPUTE ALL-INCOME = 0 
           END-IF 
           PERFORM READ-LINE
           .

       DISPLAY-PROVINCE.
           DISPLAY "  " PROVINCE-ID "      $" ALL-INCOME "    " 
      -     MAX-MEMBER "    $" MAX-MEMBER-INCOME
           .

           

       